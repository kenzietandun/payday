#!/usr/bin/env python3

import json
import os

IPP_FILE = '/etc/openvpn/ipp.txt'
OVPN_STATUS = '/etc/openvpn/openvpn-status.log'
LOGFILE = '/root/vpn.logfile'

# Create logfile if not exist
if not os.path.isfile(LOGFILE):
    os.system('touch {}'.format(LOGFILE))

# Add all users defined in IPP_FILE
users = set()
with open(IPP_FILE, 'r') as f:
    for line in f:
        user = line.split('-')[0]
        users.add(user)

# Try to load data from logfile, if logfile is empty
# create a new one with data from users set above
json_l = []
with open(LOGFILE, 'r') as f:
    try:
        usage_datas = json.load(f)
    except:
        print('Empty file')
        for user in users: 
            json_data = { 'user': user, 'value': 0 }
            json_l.append(json_data)
            with open(LOGFILE, 'w') as f:
                json.dump(json_l, f, indent=4)

# Check if user is using VPN, if yes, add to their usage stats
json_l = []
for usage_data in usage_datas:
    user = usage_data['user']
    usage = usage_data['value']
    with open(OVPN_STATUS, 'r') as f:
        for line in f:
            if ',' in line and line.split(',')[1].startswith(user):
                json_data = { 'user': user, 'value': usage+1 }
                break
        else:
            json_data = { 'user': user, 'value': usage }
    json_l.append(json_data)
    users.discard(user)

# Check if there's a new user using the service, if so, add 
# the new user to the monitored list
if users:
    for user in users:
        json_l.append({'user': user, 'value': 0})

# Write the list to logfile again for future reading
with open(LOGFILE, 'w') as f:
    json.dump(json_l, f, indent=4)
