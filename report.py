#!/usr/bin/env python3

import os
import json
import configparser

c = configparser.ConfigParser()
HERE = os.path.dirname(os.path.abspath(__file__))

def main():
    c.read(HERE + '/payday.conf')

    LOGFILE = '/root/vpn.logfile'
    with open(LOGFILE, 'r') as f:
        logs = json.load(f)

    for log in logs:
        user = log['user']
        value = log['value']
        try:
            email = c.get('email', user)
            sendmail(user, email, value)
            print('Sent report to {user}, {email}'.format(user=user, email=email))
        except:
            print('{}: Email not found'.format(user))

def sendmail(user, email, value):
    c.read(HERE + '/email.conf')
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    import smtplib

    gmail_user = c.get('email', 'me')
    gmail_pwd  = c.get('email', 'pass')
    recipient = email
    message = '''Dear {user},
<br>
<br>
<br>
<hr>
<h3>
Thank you for your loyalty and I wish you fun with my service further on! Enjoy surfing unrestricted and free internet well according to Net Neutrality which you can read more about <a href='https://en.wikipedia.org/wiki/Network_neutrality'>HERE</a>
</h3>
<br>
<br>
Your usage for this month is

<h1>{value} minutes of FREEDOM</h1>

<br>
<br>
<br>
<br>
<h3>Other Services</h3>
I also provide BaaS (Backup-as-a-Service) backed with secure, reliable storage and fast connection for Rp. 150.000,00-per-50GB. <a href='https://www.linkedin.com/pulse/you-dont-need-backup-until-do-tim-bartilla'>Why Backup?</a>
<br>
<br>

Best regards,
<br>
Kenzie
<br>
Mail: nudnateiznek@gmail.com
WhatsApp: +62811969410
'''.format(user=user.upper(), value=value)

    msg = MIMEMultipart()
    msg['From'] = gmail_user
    msg['To'] = recipient
    msg['Subject'] = 'VPN Report for {}'.format(user.upper())
    msg.attach(MIMEText(message, 'html'))

    mailServer = smtplib.SMTP('smtp.gmail.com', 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login(gmail_user, gmail_pwd)
    mailServer.sendmail(gmail_user, recipient, msg.as_string())
    mailServer.close()

if __name__ == '__main__':
    main()
